FROM nginx

RUN apt-get update -y && \
      apt-get install -y fcgiwrap git git-core

RUN mkdir -p /srv/git && \
      cd /srv/git && \
      git clone https://gitlab.com/dsearles/stringutilpublic.git stringutil && \
      cd stringutil && \
      git tag v0.0.1


EXPOSE 443

COPY ca.crt .
COPY ssl-test.godeps.bundle.crt /etc/nginx/
COPY ssl-test.godeps.key /etc/nginx/
COPY nginx.conf /etc/nginx/

COPY entrypoint.sh .
CMD ./entrypoint.sh
